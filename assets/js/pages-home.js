window.pageInit = function() {
    requirejs(['domReady', 'splide'], function(domReady, util) {
        domReady(function() {
            var analysisMarket, i,
                analysisCarousel = document.getElementsByClassName('marketAnalysisCarousel');
            for (i = 0; i < analysisCarousel.length; i++) {
                analysisMarket = analysisCarousel[i].id;
                window[analysisMarket] = new Splide('#' + analysisMarket, {
                    perPage: 4,
                    rewind: true,
                    gap: '1rem',
                    breakpoints: {
                        '1200': {
                            perPage: 3,
                            gap: '1rem',
                        },
                        '992': {
                            perPage: 4,
                            gap: '1rem',
                        },
                        '768': {
                            perPage: 3,
                            gap: '1rem',
                        },
                        '550': {
                            perPage: 1,
                            gap: '1rem',
                        },
                        '0': {
                            perPage: 1,
                            gap: '1rem',
                        }
                    }
                }).mount();
            }

            window.slider = new Splide('#homeSliderContent', {
                type: 'fade',
                rewind: true,
                slideFocus: false,
                arrows: false,
                pagination: false,
                keyboard: false
            }).mount();

            var announceLine = document.getElementById('announceLine');
            if (announceLine !== null) {
                new Splide('#announceLine', {
                    direction: 'ttb',
                    height: '24px',
                    type: 'loop',
                    autoplay: true,
                    pagination: false
                }).mount();
            }

            window.slider.on('moved', function(selectedSlider) {
                var elements = document.getElementsByClassName('sliderObject');

                for (i = 0; i < elements.length; i++) {
                    if (elements[i].dataset.slider == selectedSlider) {
                        elements[i].classList.add("active");
                    } else {
                        elements[i].classList.remove("active");
                    }
                }
            });


            var homePlatformTabs = document.getElementsByClassName("homePlatformTab");
            var platformVisual, visualTargetID, visualTarget;
            var homePlatformTabVisuals = function() {
                visualTargetID = this.getAttribute("data-visual");
                visualTarget = document.getElementById(visualTargetID);

                platformVisual = document.getElementsByClassName("platformVisual");
                for (i = 0; i < platformVisual.length; i++) {
                    if (platformVisual[i].id !== visualTargetID) {
                        if (platformVisual[i].classList.contains('active')) {
                            platformVisual[i].classList.remove('active');
                        }
                    }
                    //platformVisual[i].addEventListener('click', homePlatformTabVisuals, false);
                }
                visualTarget.classList.add('active');
            };

            if (homePlatformTabs !== undefined) {
                for (i = 0; i < homePlatformTabs.length; i++) {
                    homePlatformTabs[i].addEventListener('click', homePlatformTabVisuals, false);
                }
            }

            /*
            if( $Lc('#gcmYatirimRegulationAlert').length > 0 && getRegCookie('homepageRegulationAlert-04062020') === ''){
                requirejs(['fancybox'], function(fancybox) {
                    var $ = $Lc;
                    $.fancybox.defaults.hash = false;

                    $.fancybox.open({
                        btnTpl: {
                            close:'',
                        },
                        src  : '#gcmYatirimRegulationAlert',
                        afterShow: function( instance, slide ) {
                            setRegCookie('homepageRegulationAlert-04062020', 'yes', 365);
                        }
                    });
                });
            }
            */


            //platformTab

        });
    });
};

function engineExplanation(f) {
    if (f === 'show') {
        document.getElementById("shortEnExButton").style.display = 'block';
        document.getElementById("longEnEx").style.display = 'none';
    } else {
        document.getElementById("shortEnExButton").style.display = 'none';
        document.getElementById("longEnEx").style.display = 'block';
    }
}

function setRegCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getRegCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


//
// Slider Tv Ad Extention;
//

function openTvAd() {
    var sliderContainer = $Lc('.homeSliderWrap');


    if ($Lc('.TraderSelect').is(':visible')) {
        $Lc('.TraderSelect').val('4|TRY|/account').change();
    }

    var $ = $Lc;

    if (!sliderContainer.hasClass('tvAdSliderBuilt')) {
        var tvAddBulk;
        sliderContainer.addClass('tvAdSliderBuilt');
        $Lc('.homeFeaturedArea').addClass('tvAdOpened');

        tvAddBulk = '<div class="tvadContainer row">';
        tvAddBulk += '<button class="closeTvAdButton" onclick="closeTvAd()"><svg width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 2L17 17" stroke="white" stroke-width="3" stroke-linecap="round"/><path d="M17 2L2 17" stroke="white" stroke-width="3" stroke-linecap="round"/></svg></button>';
        tvAddBulk += '<div class="tvMockup"><div class="tvMockupHolder"><img class="tv2x1Holder" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAABCAQAAABeK7cBAAAAC0lEQVR42mNkAAIAAAoAAv/lxKUAAAAASUVORK5CYII=">';
        tvAddBulk += '<div class="tvadIframe"><div id="tvAdYoutube"></div></div>';
        tvAddBulk += '<div class="tvadCover">';
        tvAddBulk += '<span class="tvadCoverSlogan">Hemen ücretsiz deneme hesabınızı açın ve sanal para ile Borsa İstanbul’da kendinizi deneyin.</span><strong class="tvadCoverBrand">GCM Yatırım<br>Geleceğime Yatırım</strong>';
        tvAddBulk += '</div></div></div>';
        tvAddBulk += '</div>';
        sliderContainer.append(tvAddBulk);

        if (typeof(YT) == 'undefined' || typeof(YT.Player) == 'undefined') {
            requirejs(['https://www.youtube.com/iframe_api'], function(YT) {
                window.onYouTubeIframeAPIReady = function() {
                    builtTvAdYoutube();
                }
            });
        } else {
            builtTvAdYoutube();
        }
    } else {
        $Lc('.homeFeaturedArea').removeClass('tvAdEnd').addClass('tvAdOpened');
        window.tvAdYoutube.playVideo(0)
    }
}

function closeTvAd() {
    $Lc('.homeFeaturedArea').removeClass('tvAdOpened');
    window.tvAdYoutube.stopVideo()
}

function builtTvAdYoutube() {
    window.tvAdYoutube = new YT.Player('tvAdYoutube', {
        height: '300',
        width: '600',
        videoId: 'XJEqxKXqIeQ',

        playerVars: {
            cc_lang_pref: 'tr',
            rel: 0,
            loop: 0,
            modestbranding: 1,
            controls: 0,
            playsinline: 'playsinline',
            enablejsapi: 1,
            wmode: 'opaque'
        },
        origin: window.location.origin,
        enablejsapi: 1,
        widget_referrer: window.location.origin,
        events: {
            'onReady': onTvAdPlayerReady,
            'onStateChange': onTvAdPlayerStateChange
        }
    });

    function onTvAdPlayerReady(event) {
        window.tvAdYoutube.playVideo(0);
    }

    $Lc('.homeRegistrationTitleLead').click(function() {
        window.tvAdYoutube.playVideo();
    });

    function onTvAdPlayerStateChange(event) {
        if (event.data === YT.PlayerState.ENDED) {
            $Lc('.homeFeaturedArea').addClass('tvAdEnd');
        }
    }
}