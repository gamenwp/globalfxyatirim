// KNOCKOUT BINDING CONTAINER
var koContainer = '.gcmContentArea';

requirejs.config({
    paths: {
        widget: '/SERVER/bundles/widgetWithCharts?v=1',
        pjaxApi: 'libs/pjax-api/pjax-api.min',
        fontfaceobserver: 'libs/fontfaceobserver/fontfaceobserver.standalone',
        lazysizes: '/assets/bundle/lazysizes',
        splide: '/assets/js/libs/splide/splide.min',
        splideVideo: '/assets/js/libs/splide/splide-extension-video.min',
        dragscroll: '/assets/js/libs/dragscroll/dragscroll',
        highcharts: '/assets/js/libs/highcharts/highstock',
        mobileSwipe: '/assets/js/libs/mobileSwipe/mobile-swipe-menu.min',
        domReady: '/assets/js/libs/domReady/domReady.min',
        moment: '/assets/js/libs/moment/min/moment.min',
        flatpickr: '/assets/js/libs/flatpickr/flatpickr',
        select2: '/assets/js/libs/select2/js/select2.min',
        mask: '/assets/js/libs/mask/dist/jquery.mask.min',
        accountHeader: '/assets/js/myAccount-header',
        fancybox: '/assets/js/libs/fancybox/dist/jquery.fancybox.min',

    },
    map: {
        '*': {
            my: 'assets/js'
        }
    }
});

require(['lazysizes', 'dragscroll', 'mobileSwipe', 'domReady', 'splide', 'widget', 'accountHeader']);

////////////////////////////
/// scrollTo
////////////////////////////
window.scrollTop = function () {
    if ( typeof window.scrollTo === 'function'){
        window.scrollTo({ top: 0, behavior: 'smooth'});
    }else{
        document.body.scrollTop = 0; // For Safari
        document.documentElement.scrollTop = 0; // For IE
    }
}
Math.easeInOutQuad = function(t, b, c, d) {
    t /= d / 2;
    if (t < 1) { return c / 2 * t * t + b; }
    t--;
    return -c / 2 * (t * (t - 2) - 1) + b;
};

////////////////////////////
/// WebpCheck
////////////////////////////
function support_format_webp() {
    var elem = document.createElement('canvas');

    if (!!(elem.getContext && elem.getContext('2d'))) {
        // was able or not to get WebP representation
        return elem.toDataURL('image/webp').indexOf('data:image/webp') == 0;
    } else {
        // very old browser like IE 8, canvas not supported
        return false;
    }
}
var root = document.getElementsByTagName('html')[0];
if (support_format_webp()) {
    root.classList.add("webp");
} else {
    root.classList.add("no-webp");
}


////////////////////////////
/// Tab & Accordion
////////////////////////////

// Tab /- Uses "Closest" function
var ep = Element.prototype;
ep.matches = ep.matches || ep.webkitMatchesSelector || ep.msMatchesSelector || ep.mozMatchesSelector;

function closest(elem, selector) {
    if (elem !== null) {
        while (elem !== document.body) {
            elem = elem.parentElement;
            if (elem.matches(selector)) return elem;
        }
    } else { return null; }
}

var targetTab, parentTabContent, parentTabNav, relaventTabs, parentAccordion, parentAccordionItem;
document.body.addEventListener('click', function(evt) {
    if (evt.target.classList.contains('gtButton')) {
        evt.preventDefault();
        //parentTabNav = evt.target.parentNode.parentNode;
        parentTabNav = closest(evt.target, '.gtTabNav');
        targetTab = document.querySelector(evt.target.getAttribute('href'));
        parentTabContent = targetTab.parentNode;
        parentTabNav.querySelector('a.active.gtButton').classList.remove('active');
        evt.target.classList.add('active');
        relaventTabs = parentTabContent.children;
        for (var i = 0; i < relaventTabs.length; i++) {
            if (relaventTabs[i].classList.contains('active')) {
                relaventTabs[i].classList.remove('active');
            }
        }
        parentTabContent.querySelector(evt.target.getAttribute('href')).classList.add('active');
    }
}, false);

// Example Tab
/*
<div class="gtTab">
    <ul class="gtTabNav">
        <li><a class="gtButton active" href="#one">Tab 1</a></li>
        <li><a class="gtButton" href="#two">Tab 2</a></li>
    </ul>
    <div class="gtTabContent">
        <div class="gtTabPane active" id="one">Test</div>
        <div class="gtTabPane" id="two">Test2</div>
    </div>
</div>
*/

// Example Accordion
/*
<div class="gAccordion">
    <div class="gAccordionItem active">
        <div><button class="gAccordionButton">Accordion 1</button></div>
        <div class="gAccordionContent">Content</div>
    </div>
    <div class="gAccordionItem">
        <div><button class="gAccordionButton">Accordion 2</button></div>
        <div class="gAccordionContent">Content</div>
    </div>
</div>
*/

////////////////////////////
/// DropDownMenu & Accordion
////////////////////////////
var parent, gDropDownContent;
var gcDropDown = false;
document.body.addEventListener('click', function(evt) {
    if (evt.target.classList.contains('gDropDownButton')) {
        if (gcDropDown) {
            gcDropDown.classList.remove('active');
        }

        evt.preventDefault();

        parent = closest(evt.target, '.gDropDown');
        if (parent.classList.contains('active')) {
            parent.classList.remove('active');
        } else {
            gcDropDown = parent;
            parent.classList.add('active');
        }


        if (window.lazySizes) {
            if (window.lazySizes.cfg.loadMode < 3) {
                window.lazySizes.cfg.loadMode = 3;
                window.lazySizes.loader.checkElems();
            }
        }
    }
    if (gcDropDown) {
        gDropDownContent = closest(evt.target, '.gDropDown');
        if (!gDropDownContent) {
            gcDropDown.classList.remove('active');
            gcDropDown = false;
        }
    }
}, false);


/*
<div class="gDropDown">
    <button class="gDropDownButton"></button>
    <div class="gDropDownContent"></div>
</div>
* */

////////////////////////////
/// PJAX
////////////////////////////
var pjaxException;
if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > -1) {
    pjaxException = true;
}
if( pjaxException !== true){
    requirejs(['pjaxApi'], function() {
        var Pjax = require('pjaxApi').Pjax;
        window.gcmPjax =  new Pjax({
            areas: [
                // Try the first query.
                '.gcmContent',
                // Retry.
                'body'
            ],
            update: {
                css:false
            }
        });
    });
}
window.addEventListener("pjax:fetch", function() {

    // Start Loading
    document.getElementsByClassName('gcmContentArea')[0].classList.add('loading');

    // Close Header Menus
    var headerNav = document.getElementsByClassName('headerNavContainer')[0];
    if (headerNav.classList.contains("dropdownActive")) {
        headerNav.classList.remove("dropdownActive");
    }
    var body = document.getElementsByTagName('body')[0];
    if (body.classList.contains("menuOn")) {
        window.mobileMenu.close();
    }
    if (body.classList.contains("accountMenuOn")) {
        window.myAccountMobileMenu.close();
    }
});


////////////////////////////
/// PJAX & Load Inits
////////////////////////////
function triggerInit() {
    if (typeof(window.pageInit) != 'undefined') {
        // console.log('inited');
        pageInit();
        delete pageInit;
    }
}
window.addEventListener("pjax:fetch", function() {
    document.getElementsByClassName('gcmLoader')[0].classList.add('loading');
});

window.addEventListener("pjax:unload", function() {
    window.isLazySizeModeChanged = false;
    window.lazySizes.cfg.loadMode = 1;
});
document.addEventListener("pjax:ready", function() {
    //document.getElementsByClassName('gcmContentArea')[0].classList.remove('loading');
    document.getElementsByClassName('gcmLoader')[0].classList.remove('loading');

    //pageInit
    triggerInit();

});
triggerInit();

window.addEventListener("pjax:load", function() {

    if (window.scrollY > (window.innerHeight / 2)) {
        scrollTop();
    }

    // ResetDragScrolls
    if (typeof dragScrollReset === "function") {
        window.dragScrollReset();
    }


    // Knockout Reset
    window.resetKO();

    // Reload Widgets
    resetWidget();

    // Send callback to gcmUserDetailCallBack
    document.dispatchEvent(gcmUserDetailCallBack);
});


window.resetKO = function() {
    if (typeof koResetBinding === "function") {
        koResetBinding();
    }
};


window.resetWidget = function() {
    $Lc("[data-lcwidget]").html('');
    widgetsArray = [];
    initWidgets();
};

/* Widget Callbacks */

function errorFormMsgLcCallBack(cntrl, data) {

    var $ = $Lc;
    var thisEsMess = $(cntrl).parent("div");
    if (thisEsMess.find(".errorFormMessage").length == 0) {
        $(cntrl).append('<div class="row c-red errorFormMessage">' + data + "</div>");
    } else {
        $(cntrl).find(".errorFormMessage").html(data);
    }
}
var thisErValFields, tvfName;

function errorMsgLcCallBack(cntrl, data) {
    var $ = $Lc;
    thisErValFields = $(cntrl).closest(".lcFieldWrapper");
    thisErValFields.addClass('error');
    if (thisErValFields.find(".errorValidation").length == 0) {
        thisErValFields.append('<div class="errorValidation">' + data + "</div>");
    } else {
        thisErValFields.find(".errorValidation").removeClass("errorValOff");
    }
    if (thisErValFields.find(".formFeedbackIcon").length == 0) {
        thisErValFields.append('<i class="formFeedbackIcon inValidIco"></i>');
    } else {
        thisErValFields.find(".formFeedbackIcon").removeClass("ValidIco").addClass("inValidIco");
    }
}
var thisValFields;

function vaildFieldLcCallBack(cntrl) {
    var $ = $Lc;

    $(cntrl).parent("div").find(".errorValidation").addClass("errorValOff");
    thisValFields = $(cntrl).closest(".lcFieldWrapper");
    thisValFields.removeClass('error');
    if (thisValFields.find(".formFeedbackIcon").length == 0) {
        thisValFields.append('<i class="formFeedbackIcon ValidIco"></i>');
    } else {
        thisValFields.find(".formFeedbackIcon").removeClass("inValidIco").addClass("ValidIco");
    }
}

// CustomEvent PolyFill
(function() { if (typeof window.CustomEvent === "function") return false;

    function CustomEvent(event, params) { params = params || { bubbles: false, cancelable: false, detail: null }; var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail); return evt; }
    window.CustomEvent = CustomEvent; })();

// CustomEvent for gcmUserDetailCallBack
var param, gcmUserDefined;
window.gcmUserDetailCallBack = new CustomEvent("gcmUserDetail");

function userDetailsCallBack(userDetails) {
    if (gcmUserDefined !== true) {
        gcmUserDefined = true;
        document.dispatchEvent(gcmUserDetailCallBack);
    }

    // Data Layer Update - 09.03.2023
    if(userDetails.Accounts !== null && localStorage.getItem('successful_Login_event') !== 'true' && userDetails.FirstLogin == false ){
    //var Account_Status = (typeof userDetails.Accounts[0].IsDemo !== 'undefined' && userDetails.Accounts[0].IsDemo == true ) ? 'Demo' : '';
        var Account_Status = '';
        switch (userDetails.WidgetItemStatus) {
            case 1:
                Account_Status = 'Not logged in';
                localStorage.removeItem("successful_Login_event");
                break;
            case 3:
                Account_Status = 'Demo';
                break;
            case 4:
                Account_Status = 'Real';
                break;
            default:
                Account_Status = 'Not logged in';
        }
        var userAge = (typeof userDetails.DateOfBirth !== 'undefined' && userDetails.DateOfBirth !== null && userDetails.DateOfBirth !== '' ) ? getAge(userDetails.DateOfBirth) : 'Age not configured';
        //console.log(userDetails.WidgetItemStatus + ':' + Account_Status + ':' + userAge);
        var InitialLeadStatus = {
            '1': 'Potansiyel YOK',
            '2': 'GEÇERSİZ',
            '3': 'Potansiyel VAR',
            '4': 'Cevapsiz',
            '5': 'Belirlenemedi',
            '6': '50k potensiyel',
        };
        //console.log(InitialLeadStatus[userDetails.InitialLeadStatus]);
        window.dataLayer = window.dataLayer || [];
        window.dataLayer.push({
            'Age': userAge,
            'Account_Status': Account_Status,
            'Initial_Lead_Status': ( typeof InitialLeadStatus[userDetails.InitialLeadStatus] !== 'undefined' ) ? InitialLeadStatus[userDetails.InitialLeadStatus] : 'Not configured',
            'event': 'successful_Login ',
        });
        localStorage.setItem('successful_Login_event',true);
    }else if(userDetails.WidgetItemStatus == 1){
        localStorage.removeItem("successful_Login_event");
    }
    //End of Data layer Push
}
function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}
// Account Select Elements
var selectedBgClasses, isDemo, otherAccounts, active, targetTP, onceDefineSelectAccount, targetLicense, selectedMarketName;
document.addEventListener("gcmUserDetail", function(e) {
    initUserOptions();
});
function initUserOptions(){
    var $ = $Lc;
    $('.selectedAccountNumber').html(userDetails.MainAccountName);
    selectedBgClasses = []; // 0 is forex, 1 is viop on licence typs.
    selectedBgClasses[0] = 'forex';
    selectedBgClasses[1] = 'viop';
    selectedBgClasses[3] = 'opsiyon';
    selectedBgClasses[4] = 'borsa';
    selectedMarketName = []; // 0 is forex, 1 is viop on licence typs.
    selectedMarketName[0] = 'Forex';
    selectedMarketName[1] = 'Viop';
    selectedMarketName[3] = 'Opsiyon';
    selectedMarketName[4] = 'Borsa';
    isDemo = []; // false is real, true is demo.
    isDemo['true'] = 'DEMO';
    isDemo['false'] = 'REAL';

    $('.accountSelectOptions').html('');
    otherAccounts = '';
    active = '';
    $.each(userDetails.Accounts, function(key, value) {
        if (value.Name === userDetails.MainAccountName) {
            active = 'active';
            //userDetails.LoginLicense = value.License;
            $('.selectedAccountType').attr('data-bg', selectedBgClasses[value.License]);
            $('.selectedAccountType').html(isDemo[value.IsDemo]);
            $('.typeOfMarket').html(selectedMarketName[value.License]);
            $('.typeOfAccount').html(isDemo[value.IsDemo]);
            $('.selectedAccountNumber').html(value.Name);
            $('.selectedAccountCurrency').html(value.Currency);
        } else {
            active = '';
        }
        otherAccounts += '<div class="accountOption ' + active + '" data-tp="' + value.Name + '" data-license="' + value.License + '"><div class="selectedAccountType" data-bg="' + selectedBgClasses[value.License] + '">'+ isDemo[value.IsDemo] + '</div>' +'<div class="selectedAccountInfoRow">' +'<div class="selectedAccountInfo">' + '<span class="typeOfMarket">' + selectedMarketName[value.License] + '</span> - <span class="selectAccountCurrency">' + value.Currency + '</span></div>'+ '<div class="selectedTpInfo">TP <span class="selectedAccountNumber">'+ value.Name +'</span></div></div>' +'</div>';
    });
    $('.accountSelectOptions').html(otherAccounts);
    if (onceDefineSelectAccount !== true) {
        onceDefineSelectAccount = true;
        $('html').on('click', '.accountOption', function() {
            //console.log('clicked');
            if (!$(this).hasClass('active')) {
                $('.accountSelect').find('.accountOption').not($(this)).removeClass('active');
                $(this).addClass('active');

                $(this).attr('data-bg');

                targetTP = $(this).attr('data-tp');
                targetLicense = $(this).attr('data-license');
                userDetails.MainAccountName = targetTP;
                switchUser(targetTP);
                $(this).closest('.accountSelect').removeClass('active');
            }
        });
        $('html').on('click', '.accountSelect', function() {
            $(this).toggleClass('active');
        });
        $(document).bind('click', function(e) {
            if (!$(e.target).parents().hasClass("accountSelect")) {
                $(".accountSelect").removeClass('active');
            }
        });

    }
}

// After switchUser, refreshUserDetailsCallBack coming
function switchUserCallBack() {
    document.dispatchEvent(gcmUserDetailCallBack);
}

// After ajax create account, refreshUserDetailsCallBack coming
function refreshUserDetailsCallBack() {
    initUserOptions();
}