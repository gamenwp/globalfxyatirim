var onceAccountHomepage;
window.pageInit = function() {
    requirejs(['widget', 'domReady', 'splide'], function() {
        var $ = $Lc;
        var html = $('html');

        // UserNotification - Declaration
        window.jQuery = $Lc;
        requirejs(['fancybox']);

        var analysisMarket, i,
            analysisCarousel = document.getElementsByClassName('marketAnalysisCarousel');
        for (i = 0; i < analysisCarousel.length; i++) {
            analysisMarket = analysisCarousel[i].id;
            window[analysisMarket] = new Splide('#' + analysisMarket, {
                perPage: 4,
                rewind: true,
                gap: '1rem',
                breakpoints: {
                    '1200': {
                        perPage: 3,
                        gap: '1rem',
                    },
                    '992': {
                        perPage: 4,
                        gap: '1rem',
                    },
                    '768': {
                        perPage: 3,
                        gap: '1rem',
                    },
                    '550': {
                        perPage: 1,
                        gap: '1rem',
                    },
                    '0': {
                        perPage: 1,
                        gap: '1rem',
                    }
                }
            }).mount();
        }

        var announceCarousel = new Splide('.announceCarousel', {
            perPage: 4,
            rewind: true,
            gap: '1rem',
            breakpoints: {
                '5000': {
                    perPage: 3,
                    gap: '1rem',
                },
                '992': {
                    perPage: 2,
                    gap: '1rem',
                },
                '768': {
                    perPage: 1,
                    gap: '1rem',
                },
                '550': {
                    perPage: 1,
                    gap: '1rem',
                },
                '0': {
                    perPage: 1,
                    gap: '1rem',
                }
            }
        }).mount();




        var homePlatformTabs = document.getElementsByClassName("homePlatformTab");
        var platformVisual, visualTargetID, visualTarget;
        var homePlatformTabVisuals = function() {
            visualTargetID = this.getAttribute("data-visual");
            visualTarget = document.getElementById(visualTargetID);

            platformVisual = document.getElementsByClassName("platformVisual");
            for (i = 0; i < platformVisual.length; i++) {
                if (platformVisual[i].id !== visualTargetID) {
                    if (platformVisual[i].classList.contains('active')) {
                        platformVisual[i].classList.remove('active');
                    }
                }
                //platformVisual[i].addEventListener('click', homePlatformTabVisuals, false);
            }
            visualTarget.classList.add('active');
        };

        if (homePlatformTabs !== undefined) {
            for (i = 0; i < homePlatformTabs.length; i++) {
                homePlatformTabs[i].addEventListener('click', homePlatformTabVisuals, false);
            }
        }

        // !! resetKO is unBinding under body element's click events.
        if (onceAccountHomepage !== true) {
            onceAccountHomepage = true;
            html.on('click', '.notificationHide', function() {
                $('.userNotification').slideToggle(300);
            });


            html.on('click', '.updateAnnounceCarousel', function() {
                setTimeout(function() {
                    announceCarousel.mount();
                }, 200);
            });

            html.on('click', '.remainderHide', function() {
                $('.userDepositReminder').slideToggle(300);
            });

            var copyText;
            html.on('click', '.copyUserDetail', function() {
                copyText = '';
                $('.newAccountInformation .userLoginDetail').each(function() {
                    copyText += $(this).text();
                    copyText += '\r';
                });
                $('.copied').fadeIn(200).delay(700).fadeOut(200);
                copyText = copyText.trim();
                const elem = document.createElement('textarea');
                elem.value = copyText;
                document.body.appendChild(elem);
                elem.select();
                document.execCommand('copy');
                document.body.removeChild(elem);
            });

            var copyText;
            html.on('click', '.copyDeclaration', function() {
                var brRegex = /<br\s*[\/]?>/gi;
                copyText = $('.declarationText').html().replace(brRegex, "\r\n");

                $('.declarationCopied').fadeIn(200).delay(700).fadeOut(200);
                copyText = copyText.trim();
                const elem = document.createElement('textarea');
                elem.value = copyText;
                document.body.appendChild(elem);
                elem.select();
                document.execCommand('copy');
                document.body.removeChild(elem);
            });

            var cc, to, body, subject, declarationMarket;
            html.on('click', '.declarationSendEmail', function() {
                declarationMarket = $Lc(this).attr('data-market');
                $Lc.ajax({
                    url: '/SERVER/en/gcmyatirim/Client/GetDisplayAccountOwners',
                    type: 'POST',
                    context: document.body
                }).done(function(data) {
                    to = 'belge@gcmyatirim.com.tr';
                    if (declarationMarket === 'forex') {
                        if (data.ConversionOwnerEmail !== '') {
                            to = data.ConversionOwnerEmail;
                        }
                    }
                    if (declarationMarket === 'viop') {
                        if (data.ViopConversionOwnerEmail !== '') {
                            to = data.ViopConversionOwnerEmail;
                        }
                    }
                    if (declarationMarket === 'borsa') {
                        if (data.BorsaConversionOwnerEmail !== '') {
                            to = data.BorsaConversionOwnerEmail;
                        }
                    }
                    if (declarationMarket === 'opsiyon') {
                        if (data.BorsaConversionOwnerEmail !== '') {
                            to = data.BorsaConversionOwnerEmail;
                        }
                    }

                    cc = 'belge@gcmyatirim.com.tr';
                    subject = encodeURIComponent(declarationMarket.toUpperCase() + ' - Beyan Formu');
                    body = encodeURIComponent($('.declarationText').text());
                    window.open('mailto:' + to + '?cc=' + cc + '&subject=' + subject + '&body=' + body, '_blank');
                });
            });

            html.on('click', '.NotificationCloseButton', function() {
                $Lc('.NotificationWrapper').fadeOut();
            });

            var accountBorsaDay;
            html.on('click', '.accountBorsaDayTitle', function() {
                accountBorsaDay = $Lc(this).closest('.accountBorsaDay');
                if (accountBorsaDay.hasClass('hide')) {
                    accountBorsaDay.find('.accountBorsaAssetsRow').slideDown();
                    accountBorsaDay.removeClass('hide');
                } else {
                    accountBorsaDay.find('.accountBorsaAssetsRow').slideUp();
                    accountBorsaDay.addClass('hide');
                }

            });
        }

    });
};

if (typeof homePageUserFunctions === "undefined") {
    homePageUserFunctions = true;
    document.addEventListener("gcmUserDetail", function(e) {
        if ($Lc('.myAccountHome').length > 0) {
            homePageAccountInfo(userDetails);
        }
    });
}


var selectedAccount, currentLicense, selectedBg, selectedCurrency, selectedEl, selectedDemo, otherAccounts, selectedBgClasses, isDemo, oneTimeAS, targetTP, active, currentIsDemo;

var homeAccountTry = 0;

function homePageAccountInfo(userData) {

    var $ = $Lc;
    selectedAccount = userData.MainAccountName;



    $.each(userDetails.Accounts, function(key, value) {
        if (value.Name === userDetails.MainAccountName) {
            currentLicense = value.License;
            currentIsDemo = value.IsDemo;
        }

        if (value.Name === userDetails.MainAccountName && value.IsDemo === false) {

            if ((currentLicense === 0 && userData.Status === 3) || (currentLicense === 1 && userData.ViopStatus === 3) || (currentLicense === 4 && userData.SingleStockStatus === 3) || (currentLicense === 3 && userData.OptionStatus === 3)) {
                if (Number.isInteger(value.DaysForPassword)) {
                    if (value.DaysForPassword > 0 && value.DaysForPassword < 4) {

                        $Lc('.passwordDaysToExpire').html(value.DaysForPassword);

                        requirejs(['fancybox'], function(fancybox) {
                            var $ = $Lc;
                            $.fancybox.defaults.hash = false;

                            $.fancybox.open({
                                btnTpl: {
                                    close: '',
                                },
                                src: '#passwordAboutExpire'
                            });
                        });
                    }
                    if (value.DaysForPassword === 0) {
                        requirejs(['fancybox'], function(fancybox) {
                            var $ = $Lc;
                            $.fancybox.defaults.hash = false;

                            $.fancybox.open({
                                btnTpl: {
                                    close: '',
                                },
                                src: '#passwordExpired'
                            });
                        });
                    }
                }
            }
        }
    });
    if (currentLicense === 0) {
        $('.homePlatformTab, .updateAnalysisCarousel, .platformTabContent .gtTabPane, .platformVisual, .analysisTabContent .gtTabPane').removeClass('active');
        $('.homePlatformTab.forex, .updateAnalysisCarousel.forexAnalysis, #forex, #forexAnalysis, #forex-visual').addClass('active');

        if ($('.accountAreaMarketTrend').attr('data-include') !== '/modules/trend/forex') {
            $('.accountAreaMarketTrendWrapper').slideUp(function() {
                setTimeout(function() {
                    $('.accountAreaMarketTrendWrapper').show().html('<div class="row lazyload accountAreaMarketTrend" data-include="/modules/trend/forex"></div>');
                }, 1000);
            });
        }
    }
    if (currentLicense === 1) {
        $('.homePlatformTab, .updateAnalysisCarousel, .platformTabContent .gtTabPane, .platformVisual, .analysisTabContent .gtTabPane').removeClass('active');
        $('.homePlatformTab.viop, .updateAnalysisCarousel.borsaAnalysis, #viop, #borsaAnalysis, #viop-visual').addClass('active');

        $('.accountAreaMarketTrendWrapper').slideUp();
    }
    if (currentLicense === 3) {
        $('.homePlatformTab, .updateAnalysisCarousel, .platformTabContent .gtTabPane, .platformVisual, .analysisTabContent .gtTabPane').removeClass('active');
        $('.homePlatformTab.opsiyon, .updateAnalysisCarousel.forexAnalysis, #opsiyon, #forexAnalysis, #opsiyon-visual').addClass('active');
    }
    if (currentLicense === 4) {
        $('.homePlatformTab, .updateAnalysisCarousel, .platformTabContent .gtTabPane, .platformVisual, .analysisTabContent .gtTabPane').removeClass('active');
        $('.homePlatformTab.borsa, .updateAnalysisCarousel.borsaAnalysis, #borsa, #borsaAnalysis, #borsa-visual').addClass('active');

        if ($('.accountAreaMarketTrend').attr('data-include') !== '/modules/trend/borsa') {
            $('.accountAreaMarketTrendWrapper').slideUp(function() {
                setTimeout(function() {
                    $('.accountAreaMarketTrendWrapper').show().html('<div class="row lazyload accountAreaMarketTrend" data-include="/modules/trend/borsa"></div>');
                }, 1000);
            });
        }
    }

    if (currentLicense === 4 && currentIsDemo === true) {
        $('.accountSelect').removeClass('active');
        $('.accountDetailsContainer').addClass('loader');
        $('.accountDetailsContainer').hide();
    } else {
        $('.accountSelect').removeClass('active');
        $('.accountDetailsContainer').addClass('loader');
        homePageAccountTradeUpdate();
        homeAccountTry = 0;
    }


    $('.dynamicAccountNotification').slideUp();

    // Get Suitability Test Status
    $Lc.ajax({
        url: '/SERVER/en/gcmyatirim/Client/CheckNextDayForTest',
        type: 'POST',
        context: document.body
    }).done(function(suitabilityBlockStatus) {
        notificationAreaUpdate(currentLicense, userData.MainAccountName, userData, suitabilityBlockStatus);
    });
}
var market, newAccountLicence, caughted, target, licenceLevel, NewAccountServerName;

function notificationAreaUpdate(currentLicense, loggedTp, userData, suitabilityBlockStatus) {
    var $ = $Lc;
    market = 'forex';
    if (currentLicense === 0) {
        market = 'forex';
    }
    if (currentLicense === 1) {
        market = 'viop';
    }
    if (currentLicense === 3) {
        market = 'opsiyon';
    }
    if (currentLicense === 4) {
        market = 'borsa';
    }

    caughted = false;
    target = '';
    // newAccount Condition
    if (userData.NewAccountName !== '') {
        $.each(userData.Accounts, function(key, value) {

            NewAccountServerName = userData.NewAccountServer;
            if (NewAccountServerName === '0->tr') {
                NewAccountServerName = 'GCM-Demo';
            }
            if (value.Name === userData.NewAccountName) {
                if (value.License === currentLicense) {
                    caughted = true;
                    if (value.IsDemo === true) {
                        // Situation: User created demo, and in same licence login.
                        if (currentLicense !== 4) {
                            target = { 'market': market, 'notification': 'newDemo', 'newAccountName': userData.NewAccountName, 'newAccountPassword': userData.NewAccountPassword, 'newAccountServer': NewAccountServerName };
                        } else {
                            target = { 'market': market, 'notification': 'newDemo', 'newAccountName': value.EmailTp, 'newAccountPassword': userData.NewAccountPassword, 'newAccountServer': NewAccountServerName };
                        }
                    } else {
                        // Situation: User created real, and in same licence login.
                        target = { 'market': market, 'notification': 'newReal', 'newAccountName': userData.NewAccountName, 'newAccountPassword': userData.NewAccountPassword, 'newAccountServer': NewAccountServerName };
                    }
                }
            }
        });
    }
    var hasReal = false;

    // find for Real Account
    if (caughted === false) {
        if (market === 'forex') {
            $.each(userData.Accounts, function(key, value) {
                if (value.IsDemo === false && value.License === 0) {
                    hasReal = true;
                }
            });
            if (hasReal === false) {
                caughted = true;
                target = { 'market': market, 'notification': 'createReal' };
            }
        }
        if (market === 'viop') {
            $.each(userData.Accounts, function(key, value) {
                if (value.IsDemo === false && value.License === 1) {
                    hasReal = true;
                }
            });
            if (hasReal === false) {
                caughted = true;
                target = { 'market': market, 'notification': 'makeDeposit' };
            }
        }
        if (market === 'borsa') {
            $.each(userData.Accounts, function(key, value) {
                if (value.IsDemo === false && value.License === 4) {
                    hasReal = true;
                }
            });
            if (hasReal === false) {
                caughted = true;
                target = { 'market': market, 'notification': 'makeDeposit' };
            }
        }
        if (market === 'opsiyon') {
            $.each(userData.Accounts, function(key, value) {
                if (value.IsDemo === false && value.License === 4) {
                    hasReal = true;
                }
            });
            if (hasReal === false) {
                caughted = true;
                target = { 'market': market, 'notification': 'makeDeposit' };
            }
        }
    }
    // find for Deposited or Not
    if (caughted === false) {
        if (market === 'forex' && userData.FirstDeposit === false) {
            caughted = true;
            target = { 'market': market, 'notification': 'makeDeposit' };
        }
        if (market === 'viop' && userData.ViopFirstDeposit === false) {
            caughted = true;
            target = { 'market': market, 'notification': 'makeDeposit' };
        }
        if (market === 'borsa' && userData.SingleStockFirstDeposit === false) {
            caughted = true;
            target = { 'market': market, 'notification': 'makeDeposit' };
        }
        if (market === 'opsiyon' && userData.OptionFirstDeposit === false) {
            caughted = true;
            target = { 'market': market, 'notification': 'makeDeposit' };
        }
    }

    if (caughted === false) {
        if ((market === 'forex' && userData.Status !== 3) || (market === 'viop' && userData.ViopStatus !== 3) || (market === 'borsa' && userData.SingleStockStatus !== 3) || (market === 'opsiyon' && userData.OptionStatus !== 3)) {
            caughted = true;
            target = { 'market': market, 'notification': 'accountActivation' };
        }
    }

    // find for Real Verified Client
    if (caughted === false) {
        if (market === 'forex' && userData.Status === 3) {
            caughted = true;
            target = { 'market': market, 'notification': 'readyTrade' };
        }
        if (market === 'viop' && userData.ViopStatus === 3) {
            caughted = true;
            target = { 'market': market, 'notification': 'readyTrade' };
        }
        if (market === 'borsa' && userData.SingleStockStatus === 3) {
            caughted = true;
            target = { 'market': market, 'notification': 'readyTrade' };
        }
        if (market === 'opsiyon' && userData.OptionStatus === 3) {
            caughted = true;
            target = { 'market': market, 'notification': 'readyTrade' };
        }
    }

    if (target !== '') {
        // fx-newDemo, fx-createReal, fx-newReal, fx-makeDeposit, fx-contract, fx-suitability, fx-uploadDocument, fx-readyTrade
        $Lc.ajax({
            url: window.gcm.notificationSource,
            type: 'POST',
            data: target,
            context: document.body
        }).done(function(response) {
            $Lc('.dynamicAccountNotification').html(response);
            $('.dynamicAccountNotification').slideDown();
        });
    }
}

var selectedTradeInfo, selectedCurrency, selectedAccountMarket, selectedAccountType, selectedAccountServer, refreshHomeTrend, selectedServerName;
var homeTradeStatus = false;

function homePageAccountTradeUpdate() {
    homeTradeStatus = false;
    clearInterval(refreshHomeTrend);
    if ($Lc('.accountDetailsContainer').length === 0) { clearInterval(refreshHomeTrend); return; }

    $Lc.ajax({
        url: "/SERVER/en/gcmforex/ClientOptimize/GetUserTradInfo",
        context: document.body
    }).done(function(response) {
        //console.log(response);

        $Lc.each(userDetails.Accounts, function(key, value) {
            if (value.Name === userDetails.MainAccountName) {
                selectedAccountServer = value.ServerId;
            }
        });
        $Lc.each(response.TradeAccounts, function(key, value) {
            selectedCurrency = value.Currency;
            if (value.Currency === 'EUR') {
                selectedCurrency = '€';
            }
            if (value.Currency === 'USD') {
                selectedCurrency = '$';
            }
            if (value.Currency === 'TRY') {
                selectedCurrency = '₺';
            }
            if (userDetails.MainAccountName === value.Login) {
                selectedAccountMarket = value.License;

                // Forex or Viop or Opsiyon
                if (value.License === 1 || value.License === 0 || value.License === 3) {
                    $Lc('.accountDetailsContainer').show();
                    homeTradeStatus = true;
                    if (value.License === 0) {
                        selectedAccountMarket = 'Forex';
                    }
                    if (value.License === 1) {
                        selectedAccountMarket = 'Viop';
                    }
                    if (value.License === 3) {
                        selectedAccountMarket = 'Opsiyon';
                    }
                    if (value.IsDemo === false) {
                        selectedAccountType = 'Real';
                    }
                    if (value.IsDemo === true) {
                        selectedAccountType = 'Demo';
                    }

                    selectedServerName = "GCM";
                    if (typeof gcm.servers[selectedAccountServer] !== 'undefined') {
                        selectedServerName = gcm.servers[selectedAccountServer];
                    }

                    $Lc('.selectedAccountMarket').html(selectedAccountMarket);
                    //$Lc('.selectedAccountType').html(selectedAccountType);
                    //$Lc('.typeOfMarket').html(selectedMarketName[value.License]);
                    //$Lc('.typeOfAccount').html(selectedAccountType);
                    $Lc('.selectedAccountCurrency').html(value.Currency);
                    $Lc('.selectedAccountServer').html(selectedServerName);
                    $Lc('.selectedAccountBalance').html(value.Balance.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + ' ' + selectedCurrency);
                    $Lc('.selectedAccountPnl').html((value.Equity - (value.Balance + value.Credit)).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + ' ' + selectedCurrency);
                    $Lc('.selectedAccountEquity').html(value.Equity.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + ' ' + selectedCurrency);
                    $Lc('.selectedAccountUsedMargin').html(value.Margin.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + ' ' + selectedCurrency);
                    if (selectedAccountMarket === 'Viop') {
                        /*$Lc('.situationTitleViop').html('Teminat Seviyesi');*/
                        $Lc('.selectedAccountAvailableMargin').html(value.MarginLevel.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + ' ' + selectedCurrency);
                    } else {
                        /*  $Lc('.situationTitleViop').html('Kullanılabilir Teminat');*/
                        $Lc('.selectedAccountAvailableMargin').html(value.Equity.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,') + ' ' + selectedCurrency);
                    }

                    selectedTradeInfo = value;

                    var financialPower = $Lc('.financialPower');
                    var margin = value.Margin;
                    var balance = value.Balance;
                    var marginCall = 50;
                    var equity = value.Equity;

                    financialPower.removeClass('power1 power2 power3 power4 power5')
                    var marginPercentage = equity * 100;
                    if (margin != 0)
                        marginPercentage = (equity / margin) * 100;
                    if (balance == 0 && margin == 0) {
                        financialPower.addClass('power1')
                    } else if (margin == 0 && balance > 0) {
                        financialPower.addClass('power5');
                    } else if (marginPercentage <= (1 * marginCall)) {
                        financialPower.addClass('power1');
                    } else if (marginPercentage <= (2 * marginCall)) {
                        financialPower.addClass('power2');
                    } else if (marginPercentage <= (4 * marginCall)) {
                        financialPower.addClass('power3');
                    } else if (marginPercentage <= 10 * marginCall) {
                        financialPower.addClass('power4');
                    } else {
                        financialPower.addClass('power5');
                    }

                    $Lc('.accountDetailsContainerInside').hide();
                    $Lc('.accountDetailsContainerInside.forexViop').show();
                }
                // Borsa (SingleStock)
                if (value.License === 4) {
                    homeTradeStatus = true;
                    if (value.IsDemo === true) {
                        $Lc('.accountDetailsContainer').hide();
                    } else {
                        $Lc('.tBalance').html(value.Borsa.Balance + ' ₺');
                        $Lc('.tEquity').html(value.Borsa.Equity + ' ₺');
                        $Lc('.tOverDraft').html(value.Borsa.Overdraft + ' ₺');
                        $Lc('.tOverAll').html(value.Borsa.OverAll + ' ₺');
                        $Lc('.t1Balance').html(value.Borsa.T1Balance + ' ₺');
                        $Lc('.t1Equity').html(value.Borsa.T1Equity + ' ₺');
                        $Lc('.t1OverDraft').html(value.Borsa.T1Overdraft + ' ₺');
                        $Lc('.t1OverAll').html(value.Borsa.T1OverAll + ' ₺');
                        $Lc('.t2Balance').html(value.Borsa.T2Balance + ' ₺');
                        $Lc('.t2Equity').html(value.Borsa.T2Equity + ' ₺');
                        $Lc('.t2OverDraft').html(value.Borsa.T2Overdraft + ' ₺');
                        $Lc('.t2OverAll').html(value.Borsa.T2OverAll + ' ₺');


                        var t = new Date();
                        var tstring = ("0" + t.getDate()).slice(-2) + "-" + ("0" + (t.getMonth() + 1)).slice(-2) + "-" + t.getFullYear();
                        $Lc('.tDay').html(tstring + ' (T)');

                        $Lc('.accountDetailsContainer').show();
                        $Lc('.accountDetailsContainerInside').hide();
                        $Lc('.accountDetailsContainerInside.borsa').show();
                    }
                }

                var d = new Date();
                var datestring = ("0" + d.getDate()).slice(-2) + "-" + ("0" + (d.getMonth() + 1)).slice(-2) + "-" +
                    d.getFullYear() + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);
                $Lc('.accountDetailsContainer').find('.updateTimeContentField').html(datestring);

                $Lc('.accountDetailsContainer').removeClass('loader');
            }
        });

        // Retry After 10 sec
        if (homeTradeStatus === false) {
            refreshHomeTrend = setInterval(homePageAccountTradeUpdate, 10000);
        } else {
            clearInterval(refreshHomeTrend);
        }

        homeAccountTry++;
        if (homeAccountTry > 5) {
            clearInterval(refreshHomeTrend);
            $Lc('.accountDetailsContainer').hide();
        }
    });
}