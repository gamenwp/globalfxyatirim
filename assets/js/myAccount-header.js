var menuButton = document.getElementsByClassName('gcmMainMenu')[0];
var myAccountMenuButton = document.getElementsByClassName('mobileMenu')[0];
var dropDownCover = document.getElementsByClassName('dropDownCover')[0];
var myAccountCover = document.getElementsByClassName('myAccountCover')[0];
var body = document.getElementsByTagName('body')[0];

//var menu = document.getElementById('menu');
requirejs(['mobileSwipe', 'domReady'], function(mobileSwipe, domReady) {
    domReady(function() {
        // addClass
        function addClass(elem, className) {
            if (!hasClass(elem, className)) {
                elem.className += ' ' + className;
            }
        }
        // removeClass
        function removeClass(elem, className) {
            var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
            if (hasClass(elem, className)) {
                while (newClass.indexOf(' ' + className + ' ') >= 0) {
                    newClass = newClass.replace(' ' + className + ' ', ' ');
                }
                elem.className = newClass.replace(/^\s+|\s+$/g, '');
            }
        }
        // toggleClass
        function toggleClass(elem, className) {
            var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, " ") + ' ';
            if (hasClass(elem, className)) {
                while (newClass.indexOf(" " + className + " ") >= 0) {
                    newClass = newClass.replace(" " + className + " ", " ");
                }
                elem.className = newClass.replace(/^\s+|\s+$/g, '');
            } else {
                elem.className += ' ' + className;
            }
        }
        // hasClass
        function hasClass(elem, className) {
            return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
        }

        window.mobileMenu = new MobileSwipeMenu('.headerNavWrapper', {
            mode: 'right',
            width: 300,
            hookWidth: 15,
            events: {
                opening: function() {
                    addClass(body, 'menuOn');
                    //console.log(this, 'Opened');
                },
                closing: function() {
                    removeClass(body, 'menuOn');
                    //console.log(this, 'Closed');
                }
            }
        });
        window.myAccountMobileMenu = new MobileSwipeMenu('.accountHeaderNavWrapper', {
            mode: 'left',
            width: 300,
            hookWidth: 15,
            events: {
                opening: function() {
                    addClass(body, 'accountMenuOn');
                    //console.log(this, 'Opened');
                },
                closing: function() {
                    removeClass(body, 'accountMenuOn');
                    //console.log(this, 'Closed');
                }
            }
        });
        menuButton.onclick = function() {
            toggleClass(body, 'menuOn');
            if (hasClass(body, 'menuOn')) {
                addClass(body, 'menuOn');
                mobileMenu.open();
            } else {
                removeClass(body, 'menuOn')
                mobileMenu.close();
            }
            return false;
        }
        myAccountMenuButton.onclick = function() {
            toggleClass(body, 'accountMenuOn');
            if (hasClass(body, 'accountMenuOn')) {
                addClass(body, 'accountMenuOn');
                myAccountMobileMenu.open();
            } else {
                removeClass(body, 'accountMenuOn');
                myAccountMobileMenu.close();
            }
            return false;
        }
        dropDownCover.onclick = function() {
            removeClass(body, 'menuOn');
            mobileMenu.close();
            return false;
        }
        myAccountCover.onclick = function() {
            removeClass(body, 'accountMenuOn');
            myAccountMobileMenu.close();
            return false;
        }

        var acc = document.getElementsByClassName("dropDownMobile");
        var i;
        for (i = 0; i < acc.length; i++) {
            acc[i].addEventListener("click", function(ev) {
                closeAll(ev.target);
                this.parentNode.classList.toggle("active");
                var parent = this.parentNode;

                var el = document.querySelector('.dropDownBase');
                scrollTo(el, 0, 500);
            });

        }

        function closeAll(tar) {
            var accs = document.querySelectorAll('.dropDownMobile');
            for (var i = 0; i < accs.length; i++) {
                if (accs[i] == tar) {
                    continue;
                }
                accs[i].parentNode.classList.remove('active');
            }
        }


    });
});